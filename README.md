  

# Test technique Boby

  
2 tests à réaliser

- <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/logo_boby_travel.png" alt="Boby Voyages Logo" width="100"/> Symfony : Boby voyages

-  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/logo_boby_museum.png" alt="Boby Voyages Logo" width="100"/> React : Boby Museum

___

 <img src="https://symfony.com/logos/symfony_black_03.png" alt="Symfony" width="100"/>


Boby voyages : Réalisation d’une page de voyages avec notations.

___


 React <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/512px-React-icon.svg.png" alt="React" width="100"/>



Boby Museum : Réalisation d’un mini-jeu lié à des œuvres d’art

___

Sauf contraintes particulières indiquées, vous êtes libres d’utiliser les bibliothèques, framework css que vous souhaitez (ou ne pas en utiliser du tout)
  

## Symfony - Boby Voyages

<p align="center">
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/logo_boby_travel.png" alt="Boby Voyages Logo" width="400"/>
</p>

**Contraintes techniques**

- Dernière version de Symfony
- Les données doivent provenir d’une base de donnée Mysql
- Utilisation de Twig (moteur de template par défaut de Symfony) pour le front (pas de framework JS)

___

**User stories** 

-   *En tant qu’utilisateur je souhaite afficher une liste de voyages*
    
-   *En tant qu’utilisateur je souhaite afficher le détail d’un voyage*
    
-   *En tant qu’utilisateur je souhaite consulter la note d’un voyage via un système d’étoiles*
   
   ___
   **Wireframes**
<p align="center">
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/wireframe_boby_travel.png" alt="Boby Voyages wireframe" width="800"/>
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/wireframe_boby_travel_detail.png" alt="Boby Voyages Wireframe details" width="800"/>
</p>

___

***1/ En tant qu’utilisateur je souhaite afficher une liste de voyages***

Vous pouvez créer un jeu de données fictives dans votre base de données (vous pouvez si vous le souhaitez utiliser les [fixtures](https://symfony.com/bundles/DoctrineFixturesBundle/current/index.html#writing-fixtures))

Ne pas développer de système d’upload d’image, utiliser directement des liens absolus qui seront stockés en base de donnée.

Sur une seule page :

- Afficher tous les voyages contenus dans votre base de donnée

- Chaque carte devra comporter une photo, un titre (lieu, par exemple « Les Seychelles ») et une description (Lorem ipsum autorisé). La description complète ne sera pas affichée sur les cards (utiliser des points de suspensions lorsque la phrase est coupée).

***2/ En tant qu’utilisateur je souhaite afficher le détail d’un voyage***

- Au clic sur "Voir plus", une nouvelle page affiche le détail du voyage. Cette page présente les données complète du voyage sélectionné.

***3/ En tant qu’utilisateur je souhaite connaître la note d’un voyage via un système d’étoiles***

- La notation doit apparaître sur les cards de la page principale et également sur le détail du voyage.

- Les notes seront stockées en base de données, dans la même entité que le voyage

- Insérer des notes fictives entre 1 et 5, en ajoutant des notes comportant une décimale (exemple 3,2)

- Utiliser uniquement des étoiles pleines. Pas d’étoile remplie à moitié ou vide.

Exemple pour 3,2 : ⭐⭐⭐

Exemple pour 2 : ⭐⭐

___
___
  

## React - Boby Museum
<p align="center">
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/logo_boby_museum.png" alt="Boby Voyages Logo" width="400"/>
</p>


**Contraintes techniques**

 - Utiliser [create-react-app](https://create-react-app.dev/)
 - Ne pas développer de back-end
 - Utiliser l’[API publique de Met Museum](https://metmuseum.github.io/) (pas besoin de clé ni d’authentification)

___

**User stories**

 - *En tant qu’utilisateur je souhaite afficher une œuvre d’art*

 - *En tant qu’utilisateur je souhaite tenter de deviner l’année de
   création de l’œuvre*

 - *En tant qu’utilisateur je souhaite connaître mon résultat*
  
  ___
  **Wireframes**
<p align="center">
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/wireframe_boby_museum.png" alt="Boby Museum wireframe" width="800"/>
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/wireframe_boby_museum_success.png" alt="Boby Museum Wireframe success" width="800"/>
  <img src="https://gitlab.com/cybat/test-technique-fr/-/raw/main/images/wireframe_boby_museum_fail.png" alt="Boby Museum Wireframe fail" width="800"/>
</p>

___

 ***1/ En tant qu’utilisateur je souhaite afficher une œuvre d’art***

 [Documentation de l’API Met Museum](https://metmuseum.github.io/#endpoints)

L'application doit choisir une œuvre d’art au hasard parmi celle du département `European Paintings`
Afficher l’image et le titre de l’œuvre.
  

 ***2/ En tant qu’utilisateur je souhaite tenter de deviner la date de l’œuvre***

Insérer un champ de saisie pour saisir l’année

 ***3/ En tant qu’utilisateur je souhaite connaître mon résultat***

Exemples :

- Bravo, vous avez trouvé l’année exacte de création de cette œuvre !

- Vous avez estimé cette œuvre avec un écart de +30 ans

- Vous avez estimé cette œuvre avec un écart de -52 ans

Contrainte technique :

Utiliser la clé `objectEndDate` (1523  : [dans l’exemple de cet objet](https://collectionapi.metmuseum.org/public/collection/v1/objects/435595))

___

## Indications générales
Ne pas pousser vos solutions sur ce repository. Créez votre repository sur gitlab ou github et transmettez-nous le lien d’accès. Si votre repository est privé, ajouter les droits d'accés à 
guillaumebdx (si github)
guillaumeharari (si gitlab) 

Pour le projet Boby Voyages (Symfony) vous avez la possibilité de commencer de zéro ou de partir de ce boilerplate (à utiliser avec docker) qui contient tout le projet initialisé, avec son entité, des fixtures et les premières données affichées sur la page principale :

https://gitlab.com/cybat/technical-test-boilerplate


Crédits images : Génération Midjourney
